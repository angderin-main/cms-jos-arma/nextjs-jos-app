import type { NextPage } from "next";
import MainLayout from "../components/layouts/Main";

const Home: NextPage = () => {
  return <MainLayout></MainLayout>;
};

export default Home;
