import { Container, Grid, Skeleton, Table } from "@mantine/core";
import Modlist from "../../components/servers/Modlist";
import Status from "../../components/servers/Status";
import Template from "../../components/servers/Template";

const ServerStatusPage = () => {
  return (
    <Grid>
      <Grid.Col xs={12} sm={12} md={12} lg={4}>
        <Status />
      </Grid.Col>
      <Grid.Col xs={12} sm={12} md={12} lg={4}>
        <Modlist />
      </Grid.Col>
      <Grid.Col xs={12} sm={12} md={12} lg={4}>
        <Template />
      </Grid.Col>
    </Grid>
  );
};

export default ServerStatusPage;
