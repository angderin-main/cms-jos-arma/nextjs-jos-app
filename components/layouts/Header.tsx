import { Button, Burger, createStyles } from "@mantine/core";
import { IconSettings } from "@tabler/icons";

const useStyles = createStyles((theme, _params, getRef) => ({
  header: {
    overflow: "hidden",
    position: "fixed",
    backgroundColor: "#fff",
    top: 0,
    left: 0,
    right: 0,
    display: "flex",
    height: "3.75em",
    zIndex: 101,
    justifyContent: "space-between",
    alignItems: "center",
    textAlign: "center",
    borderBottom: `1px solid ${theme.colors.gray[2]}`,
  },
  title: {
    paddingLeft: "1em",
    paddingRight: "1em",
    alignItems: "center",
    textAlign: "center",
  },
  item: {
    paddingLeft: "1em",
    paddingRight: "1em",
    alignItems: "center",
    textAlign: "center",
  },
  button: {
    width: "32px",
    height: "32px",
  },
  headerSm: {
    overflow: "hidden",
    position: "fixed",
    backgroundColor: "#fff",
    top: 0,
    left: 0,
    right: 0,
    display: "none",
    height: "3.75em",
    zIndex: 101,
    justifyContent: "space-between",
    alignItems: "center",
    textAlign: "center",
    borderBottom: `1px solid ${theme.colors.gray[2]}`,
    [theme.fn.smallerThan("sm")]: {
      display: "flex",
    },
    paddingLeft: "1em",
    paddingRight: "1em",
  },
  itemSm: {
    paddingLeft: "1em",
    paddingRight: "1em",
    alignItems: "center",
    textAlign: "center",
  },
}));

const MainHeader = (props: {
  burgerState: boolean;
  onBurgerToggle: () => void;
}) => {
  const { classes } = useStyles();
  return (
    <>
      <div className={classes.header}>
        <div>
          <div className={classes.item}>
            <h1>JOS-ARMA</h1>
          </div>
        </div>
        <div>
          <div className={classes.item}>
            <Button radius={"md"} px={"0.5em"}>
              <IconSettings size={"32px"} stroke={1.5} />
            </Button>
          </div>
        </div>
      </div>
      <div className={classes.headerSm}>
        <Burger
          opened={props.burgerState}
          onClick={props.onBurgerToggle}
        ></Burger>
        <div className={classes.item}>
          <h1>JOS-ARMA</h1>
        </div>
        <div className={classes.item}>
          <Button radius={"md"} px={"0.5em"}>
            <IconSettings size={"32px"} stroke={1.5} />
          </Button>
        </div>
      </div>
    </>
  );
};

export default MainHeader;
