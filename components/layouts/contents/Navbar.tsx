import { NavLink } from "@mantine/core";
import { IconChevronRight, IconServer, IconUsers } from "@tabler/icons";
import Link from "next/link";
import { useRouter } from "next/router";

const NavbarContent = () => {
  const router = useRouter();
  const path = router.asPath.split("/");
  console.log("RENDER MAIN NAVBAR");
  return (
    <>
      <NavLink
        label="Server"
        icon={<IconServer size={16} stroke={1.5} />}
        rightSection={<IconChevronRight size={12} stroke={1.5} />}
        defaultOpened={path[1] === "server" ? true : false}
      >
        <Link href={"/server/status"} passHref>
          <NavLink
            label="Status"
            component="a"
            active={path[2] === "status" ? true : false}
          />
        </Link>
        <Link href={"/server/modlist"}>
          <NavLink
            label="Modlist"
            component="a"
            active={path[2] === "modlist" ? true : false}
          />
        </Link>
        <Link href={"/server/template"}>
          <NavLink
            label="Template"
            component="a"
            active={path[2] === "template" ? true : false}
          />
        </Link>
      </NavLink>
      <NavLink
        label="Event"
        icon={<IconUsers size={16} stroke={1.5} />}
        rightSection={<IconChevronRight size={12} stroke={1.5} />}
        defaultOpened={path[1] === "event" ? true : false}
      >
        <Link href={"/event/list"} passHref>
          <NavLink
            label="List"
            component="a"
            active={path[2] === "list" ? true : false}
          />
        </Link>
        <Link href={"/event/slotting"} passHref>
          <NavLink
            label="Slotting"
            component="a"
            active={path[2] === "slotting" ? true : false}
          />
        </Link>
      </NavLink>
    </>
  );
};

export default NavbarContent;
