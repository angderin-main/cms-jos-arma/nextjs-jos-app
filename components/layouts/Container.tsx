import { Container, createStyles, Grid, ScrollArea, Skeleton } from "@mantine/core";
import { ReactElement } from "react";
import MainNavbar from "./Navbar";

const child = <Skeleton height={140} radius="md" animate={true} />;

const MainContainer = (props: { children?: ReactElement }) => {
  console.log("RENDER MAIN CONTAINER")
  return (
    <Container fluid={true} pt={"4.75em"}>
      {props.children}
      {/* <Grid grow>
        <Grid.Col xs={8}>{child}</Grid.Col>
        <Grid.Col xs={4}>{child}</Grid.Col>
        <Grid.Col xs={3}>{child}</Grid.Col>
        <Grid.Col xs={6}>{child}</Grid.Col>
        <Grid.Col xs={8}>{child}</Grid.Col>
        <Grid.Col xs={4}>{child}</Grid.Col>
        <Grid.Col xs={3}>{child}</Grid.Col>
        <Grid.Col xs={6}>{child}</Grid.Col>
        <Grid.Col xs={8}>{child}</Grid.Col>
        <Grid.Col xs={4}>{child}</Grid.Col>
        <Grid.Col xs={3}>{child}</Grid.Col>
        <Grid.Col xs={6}>{child}</Grid.Col>
        <Grid.Col xs={8}>{child}</Grid.Col>
        <Grid.Col xs={4}>{child}</Grid.Col>
        <Grid.Col xs={3}>{child}</Grid.Col>
        <Grid.Col xs={6}>{child}</Grid.Col>
      </Grid> */}
    </Container>
  );
};

export default MainContainer;
