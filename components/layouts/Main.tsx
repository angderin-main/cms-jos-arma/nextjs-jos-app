import { Container, createStyles, Grid, Skeleton } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { ReactElement } from "react";
import MainContainer from "./Container";
import MainHeader from "./Header";
import MainNavbar from "./Navbar";

const useStyle = createStyles((theme, _params, getRef) => ({
  container: {
    paddingLeft: "250px",

    [theme.fn.smallerThan("sm")]: {
      paddingLeft: "1em",
    },
  },
}));

const child = <Skeleton height={140} radius="md" animate={false} />;
const MainLayout = (props: { children?: ReactElement }) => {
  const [open, { toggle, close }] = useDisclosure(false);
  const {classes} = useStyle();
  return (
    <Container fluid={true} color={"grey"}>
      <Container fluid={true} className={classes.container}>
        <MainHeader burgerState={open} onBurgerToggle={toggle} />
        <MainNavbar burgerState={open} />
        <MainContainer>{props.children}</MainContainer>
      </Container>
    </Container>
  );
};

export default MainLayout;
