import { createStyles, Navbar, NavLink, ScrollArea } from "@mantine/core";
import NavbarContent from "./contents/Navbar";

const useStyle = createStyles((theme, _params, getRef) => ({
  navbar: {
    height: "100vh",
    width: "260px",
    position: "fixed",
    paddingRight: 0,
    paddingTop: "4.75em",
    top: 0,
    left: 0,
    [theme.fn.smallerThan("sm")]: {
      display: "none",
      width: "100%",
    },
  },
  navItem: {},
}));

const MainNavbar = (props: { burgerState: boolean }) => {
  const { classes } = useStyle();
  return (
    // <nav className={classes.navbar}>

    // </nav>
    <Navbar
      className={classes.navbar}
      style={props.burgerState ? { display: "flex" } : {}}
    >
      <Navbar.Section>
        <h1>HEADER</h1>
      </Navbar.Section>
      <Navbar.Section grow component={ScrollArea} mx="-xs" px="xs">
        <NavbarContent />
      </Navbar.Section>
      <Navbar.Section>
        <h1>FOOTER</h1>
      </Navbar.Section>
    </Navbar>
  );
};

export default MainNavbar;
