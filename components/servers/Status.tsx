import { Paper, ScrollArea, Table, Text } from "@mantine/core";
const Status = () => {
  return (
    <Paper shadow={"md"} radius={"xs"} withBorder p={"md"} pt={0}>
      <h2>Server Status</h2>
      <ScrollArea style={{height:"90%"}}>
      </ScrollArea>
    </Paper>
  );
};
export default Status;
