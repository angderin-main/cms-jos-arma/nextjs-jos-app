import { Paper, ScrollArea, Table, Text } from "@mantine/core";

const DUMMY_TEMPLATE_TEXT = `./jos_daily_acre_takis_1.takistan.pbo
./OPTRE_JOS_TEST_1.OPTRE_Madrigal.pbo
./Summer%20Heist%20Festival.panthera3.pbo
./jos_scp_test_2.cytech_underground_terrain.pbo
./SAVE_Operation_Steel_Needle_E8.sara.pbo
./jos_daily_sov_1-8.takistan.pbo
./OPTRE_WINTER_SEASON.OPTRE_Madrigal.pbo
./AFOL_NORTH_MK10.Chernarus_Winter.pbo
./jos_daily_acre_chrn_at_1.chernarus.pbo
./PMC_ONE_SIDED_SIDE_E5.sara.pbo
./Gula_Donat_00.panthera3.pbo
./LIVONIA_GREATER GOOD.Enoch.pbo
./USMC_Walking_Eagle_E9.sara.pbo
./USMC_Hammering_Thunder_E6.sara.pbo
./OPTRE_UNSC_INITIATION v1.OPTRE_Madrigal.pbo
./BAD_COMPANY.abramia.pbo
./jos_test_save_1-2.rof_mok.pbo
./SHADOW COMPANY-PANTHERA.panthera3.pbo
./jos_daily_acre_malden_1.Malden.pbo
./jos_daily_acre_sharani_1.sara_dbe1.pbo
./jos_daily_acre_abramia_1-2.abramia.pbo
./JOS_SCP_CYTECH2.cytech_underground_terrain.pbo
./[SAVE]_Price_&_Joy.panthera3.pbo
./OPTRE_JOS.OPTRE_Madrigal.pbo
./AFOL_NORTH_MK11.Chernarus_Winter.pbo
./OPTRE OP_GREEN JEWEL v2.OPTRE_Madrigal.pbo
./OPTRE_OPERATION GREEN JEWEL v2.OPTRE_Madrigal.pbo
./ABRAMIA-FROM DAWN TO LIGHT v 2.abramia.pbo
./Akimbo Handshakes.panthera3.pbo
./US_DAYLIGHT_ANGEL_E4.sara.pbo
./FROSTBITTEN_SIGNALS.Chernarus_Winter.pbo
./COLDWAR_LATRIVIA_1.Woodland_ACR.pbo
./jos_daily_acre_altis_1.Altis.pbo
./jos_daily_acre_1.rof_mok.pbo
./SCP_41.NewYork_Lumnuon.pbo
./jos_daily_acre_panthera_w_1-2.Winthera3.pbo
./SONS OF SAHARA-HARBOR.sara.pbo
./JOS_TANOA_ACRE.Tanoa.pbo
./jos_daily_acre_panthera_n_1-2.panthera3.pbo
./[SAVE]_Winged_Angel_of.rof_mok.pbo
./jos_test_save_1.rof_mok.pbo
./DAYLIGHT_ANGEL_E11.sara.pbo
./US_LPoG_E10.sara.pbo
./Operation Nut Hiding V1.panthera3.pbo
./jos_daily_acre_tanoa_1.Tanoa.pbo
./anomaly_41_go_home_2.NewYork_Lumnuon.pbo
./US_Raining_Wednesday_E7.sara.pbo
./SAVE STRONGHOLD UTES_ISLAND.utes.pbo
./[SAVE]_Last_Frontier.rof_mok.pbo
`;

const Template = () => {

  const templateList = DUMMY_TEMPLATE_TEXT.replace("\n", "")
    .split("./")
    .slice(1);
  console.log(templateList);
  
  const tableBody = templateList.map((v) => (
    <tr key={v}>
      <td>{v}</td>
    </tr>
  ));

  return (
    <Paper
      shadow={"md"}
      radius={"xs"}
      withBorder
      p={"md"}
      pt={0}
      style={{ height: "90vh" }}
    >
      <h2>Template List</h2>
      <ScrollArea style={{ height: "90%" }}>
        <Table striped highlightOnHover>
          <thead>
            <tr>
              <th style={{ width: "100%" }}>Name</th>
            </tr>
          </thead>
          <tbody>{tableBody}</tbody>
        </Table>
      </ScrollArea>
    </Paper>
  );
};
export default Template;
