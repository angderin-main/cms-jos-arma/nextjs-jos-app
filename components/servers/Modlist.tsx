import { Paper, ScrollArea, Table, Text } from "@mantine/core";
import { useEffect } from "react";

const DUMMY_MODLIST = [
  {
    name: "ace",
    directory: "@ace",
    modset: "ACRE_1",
  },
  {
    name: "ace",
    directory: "@ace",
    modset: "ACRE_1",
  },
  {
    name: "ace",
    directory: "@ace",
    modset: "ACRE_1",
  },
  {
    name: "ace",
    directory: "@ace",
    modset: "ACRE_1",
  },
  {
    name: "ace",
    directory: "@ace",
    modset: "ACRE_1",
  },
  {
    name: "ace",
    directory: "@ace",
    modset: "ACRE_1",
  },
  {
    name: "ace",
    directory: "@ace",
    modset: "ACRE_1",
  },
];

const DUMMY_MODLIST_TEXT =
  "mods/@cba_a3;mods/@ace;mods/@lambs_fsm;mods/@lambs_sup;mods/@lambs_rpg;mods/@lambs_rpg_rhs;mods/@lambs_turret;mods/@zen;mods/@zen_acec;mods/@acre2;mods/@dui;mods/@suppress;mods/@grad_trenches;mods/@boc;mods/@cup_core;mods/@cup_map;mods/@cup_inter;mods/@cup_acec_map;mods/@isla_panthera;mods/@isla_abramia;mods/@rhsusaf;mods/@rhsafrf;mods/@rhsgref;mods/@rhsusaf_acec;mods/@rhsafrf_acec;mods/@rhsgref_acec;mods/@rhssaf;mods/@rhssaf_acec;mods/@3cbf;mods/@jos_tni;mods/@jos_opfor;mods/@bwi_pindad;mods/@niarms_aio_v14;mods/@niarms_aio_v14_acec;mods/@niarms_aio_v14_rhsc;mods/@bettir;mods/@boxloader;mods/@boxloader_acec;mods/@mrh;mods/@ctab_devastator;mods/@sa_ar18;mods/@fm12;mods/@fm12_acec";

const Modlist = () => {
  let temp2: { name: string; directory: string; modset: string }[] = [];
  const temp1 = DUMMY_MODLIST_TEXT.split(";");

  temp2 = temp1.map((v) => {
    let temp3 = v;
    let temp4 = v.split("@");

    return {
      name: temp4[1],
      directory: temp3,
      modset: "ACRE_1",
    };
  });

  const modlist = temp2;

  const tableBody = modlist.map((v) => (
    <tr key={v.name}>
      <td>{v.name}</td>
      <td>{v.directory}</td>
      <td>{v.modset}</td>
    </tr>
  ));

  useEffect(() => {
    temp2 = temp1.map((v) => {
      let temp3 = v;
      let temp4 = v.split("@");

      return {
        name: temp4[1],
        directory: temp3,
        modset: "ACRE_1",
      };
    });
  }, []);

  return (
    <Paper
      shadow={"md"}
      radius={"xs"}
      withBorder
      p={"md"}
      pt={0}
      style={{ height: "90vh" }}
    >
      <h2>{`Current Modlist - ${"ACRE_1"}`}</h2>
      <ScrollArea style={{ height: "90%" }}>
        <Table striped highlightOnHover>
          <thead>
            <tr>
              <th style={{ width: "30%" }}>Name</th>
              <th style={{ width: "50%" }}>Directory</th>
              <th style={{ width: "20%" }}>Modset</th>
            </tr>
          </thead>
          <tbody>{tableBody}</tbody>
        </Table>
      </ScrollArea>
    </Paper>
  );
};
export default Modlist;
